﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.Reflection;

namespace H1Z1_ConfigTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Dictionary<string, List<keyBindInfo>> ActionSets = new Dictionary<string, List<keyBindInfo>>();
        ObservableCollection<dataGridClass> empData = new ObservableCollection<dataGridClass>();
        public ListCollectionView collection;
        public MainWindow()
        {
            InitializeComponent();
            SaveXML.IsEnabled = false;
            this.Title = this.Title + " v" + Assembly.GetExecutingAssembly().GetName().Version.ToString();
           
            findXMLfile.CheckFileExists = true;
            findXMLfile.DefaultExt = ".xml";
            findXMLfile.Filter = "Input xml|InputProfile_User.xml";
            findXMLfile.RestoreDirectory = true;

            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.path))
                findXMLfile.InitialDirectory = Properties.Settings.Default.path;
            else
                findXMLfile.InitialDirectory = steamInstallPath();
            collection = new ListCollectionView(empData);
            collection.GroupDescriptions.Add(new PropertyGroupDescription("ActionSet"));
            dataGrid.ItemsSource = collection;
        }

        public static string steamInstallPath()
        {
            string path;
            try
            {
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Valve\\Steam"))
                {
                    if (key == null)
                        using (RegistryKey key32 = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Valve\\Steam"))
                        {
                            if (key32 == null)
                                return null;
                            else
                            {
                                path = System.IO.Path.Combine(key32.GetValue("InstallPath", null).ToString(),"SteamApps\\common\\h1z1");
                                if (Directory.Exists(path))
                                    return path;
                                else
                                    return key32.GetValue("InstallPath", null).ToString();
                            }
                        }
                     path = System.IO.Path.Combine(key.GetValue("InstallPath", null).ToString(),"SteamApps\\common\\h1z1");
                     if (Directory.Exists(path))
                         return path;
                     else
                         return key.GetValue("InstallPath", null).ToString();

                }

            }
            catch
            {
                return null;
            }
        }
        public ObservableCollection<dataGridClass> controlData = new ObservableCollection<dataGridClass>();
        public void parseXML(string path)
        {
            ActionSets.Clear();
            
            if (!File.Exists(path))
            {
                MessageBox.Show(string.Format("File {0} doesnt exist", path));
                return;
            }
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/Profile/ActionSet");
            List<keyBindInfo> keys = new List<keyBindInfo>();
            foreach (XmlNode node in nodes)
            {
                
                    keyBindInfo key = new keyBindInfo();
                    key.ActionSet = node.Attributes["name"].Value;

                    XmlNodeList actionNodes = node.ChildNodes;
                    foreach (XmlNode actionNode in actionNodes)
                    {
                        ActionTriggerData data = new ActionTriggerData();
                        data.Action = actionNode.Attributes["name"].Value;
                        if (actionNode.SelectSingleNode("Trigger") == null)
                            data.TriggerA = "None";
                        else
                        {
                            string trigger = actionNode.SelectSingleNode("Trigger").InnerText;
                            string[] triggers = trigger.Split('+');
                            data.TriggerA = triggers[0];
                            if (triggers.Length < 2)
                                data.TriggerB = "None";
                            else
                                data.TriggerB = triggers[1];
                            
                        }
                        key.ActionTrigger.Add(data);
                       empData.Add(new dataGridClass{ActionSet=key.ActionSet, Action=data.Action,TriggerKeyA=data.TriggerA, TriggerKeyB=data.TriggerB});
                    }
                    keys.Add(key);
                    ActionSets.Add(key.ActionSet, keys);
            }
            dataGrid.ItemsSource = collection;
            //createXML(keys);
        }
        public void createXML(string ActionSet, List<ActionTriggerData> keyList)
        {
            
            //XmlDocument doc = new XmlDocument();
            //XPathNavigator nav = doc.CreateNavigator();

            //using (XmlWriter w = nav.AppendChild())
            //{

            //        XmlSerializer ser = new XmlSerializer(typeof(List<keyBindInfo>));
            //        ser.Serialize(w, keyList);
            //        MessageBox.Show(w.ToString());

            //}
            //string header = "<Profile name=\"User\" version=\"3\">\n<ActionSet name=\"Generic\">";
            //string footer = "</ActionSet>\n</Profile>";
            string line1 = "<ActionSet name=\"" + ActionSet + "\">";
            string line3 = "</ActionSet>";
            string line2 = "";
            foreach (ActionTriggerData key in keyList)
            {
                string keyToPress ="";
                if (!key.TriggerB.Contains("None"))
                    keyToPress = string.Format("{0}+{1}", key.TriggerA, key.TriggerB);
                else
                    keyToPress = key.TriggerA;


                string newLine = new XElement("Action",
                    new XAttribute("name", key.Action),
                    new XElement("Trigger", keyToPress)).ToString();
                if (string.IsNullOrWhiteSpace(line2))
                    line2 = newLine;
                else
                    line2 = string.Format("{0}\n{1}", line2, newLine);


            }
           // MessageBox.Show(string.Format("{0}\n{1}\n{2}", line1, line2, line3));
            writeToFile(line1);
            writeToFile(line2);
            writeToFile(line3);
            
        }
        public void writeToFile(object text)
        {
            using (StreamWriter write = new StreamWriter(filePath, true))
            {
                write.WriteLine(text);
            }

        }


        public static string filePath;
        public Microsoft.Win32.OpenFileDialog findXMLfile = new Microsoft.Win32.OpenFileDialog();
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            empData.Clear();
            filePath = null;
            pleasewait.Visibility = System.Windows.Visibility.Visible;
            if (string.IsNullOrWhiteSpace(filePath))
                if (findXMLfile.ShowDialog() ?? false)
                {
                    filePath = findXMLfile.FileName;
                }
                else
                {
                    MessageBox.Show("No File selected! Try again.");
                    pleasewait.Visibility = System.Windows.Visibility.Hidden;
                    return;
                }

           parseXML(filePath);
            //test();
           pleasewait.Visibility = System.Windows.Visibility.Hidden;
           SaveXML.IsEnabled = true;
        }

        private void SaveXML_Click(object sender, RoutedEventArgs e)
        {
            pleasewait.Visibility = System.Windows.Visibility.Visible;
          
            List<ActionTriggerData> Generic = new List<ActionTriggerData>();
            List<ActionTriggerData> QuickChat =new List<ActionTriggerData>();
            List<ActionTriggerData> GroundVehicle = new List<ActionTriggerData>();
            List<ActionTriggerData> Aircraft = new List<ActionTriggerData>();
            List<ActionTriggerData> Infantry = new List<ActionTriggerData>();
            List<ActionTriggerData> FreeFly = new List<ActionTriggerData>();

          foreach(var item in empData)
          {
              
              ActionTriggerData rowItem = new ActionTriggerData {Action = item.Action,TriggerA =item.TriggerKeyA,TriggerB = item.TriggerKeyB};
              switch (item.ActionSet)
              {
                  case "Generic":
                      Generic.Add(rowItem);
                      break;
                  case "QuickChat":
                      QuickChat.Add(rowItem);
                      break;
                  case "GroundVehicle":
                      GroundVehicle.Add(rowItem);
                      break;
                  case "Aircraft":
                      Aircraft.Add(rowItem);
                      break;
                  case "Infantry":
                      Infantry.Add(rowItem);
                      break;
                  case "FreeFly":
                      FreeFly.Add(rowItem);
                      break;
                  default:
                      break;
              }              
          }
          string fileSafeTimestamp = "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
          File.Move(filePath, filePath + fileSafeTimestamp);
          writeToFile("<Profile name=\"User\" version=\"3\">");
          
          createXML("Generic", Generic);
          createXML("QuickChat", QuickChat);
          createXML("GroundVehicle", GroundVehicle);
          createXML("Aircraft", Aircraft);
          createXML("Infantry", Infantry);
          createXML("FreeFly", FreeFly);
          writeToFile("</Profile>");
          pleasewait.Visibility = System.Windows.Visibility.Hidden;
        }

        private void donate_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DPY5MKXNAMLAY");
        }
        
    }
   

    [Serializable]
    public class keyBindInfo
    {
        public string ActionSet;
        public List<ActionTriggerData> ActionTrigger = new List<ActionTriggerData>();
    }

    [Serializable]
    public class ActionTriggerData
    {
        public string Action;
        public string TriggerA;
        public string TriggerB = "None";
    }
    public class dataGridClass
    {
        public string ActionSet { get; set; }
        public string Action { get; set; }
        public string TriggerKeyA { get; set; }
        public string TriggerKeyB { get; set; }

    }
}
