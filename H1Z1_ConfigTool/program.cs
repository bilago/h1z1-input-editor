﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace H1Z1_ConfigTool
{
    class program
    {
        [STAThread]
        static void Main(string[] args)
        {
            H1Z1_ConfigTool.App app = new H1Z1_ConfigTool.App();

            EmbeddedAssembly.Load("H1Z1_ConfigTool.System.Windows.Controls.Input.Toolkit.dll", "System.Windows.Controls.Input.Toolkit.dll");
            EmbeddedAssembly.Load("H1Z1_ConfigTool.System.Windows.Controls.Layout.Toolkit.dll", "System.Windows.Controls.Layout.Toolkit.dll");
            EmbeddedAssembly.Load("H1Z1_ConfigTool.WPFToolkit.dll", "WPFToolkit.dll");
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            app.InitializeComponent();
            app.Run();

        }
        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return EmbeddedAssembly.Get(args.Name);
        }
    }
}
